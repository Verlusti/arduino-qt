# arduino-qt

## Setup arduino-cli

Download current arduino-cli alpha preview
 -> https://github.com/arduino/arduino-cli
 
Example for Linux 64 bit

wget https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-linux64.tar.bz2
tar -xvf tar xvf arduino-cli-latest-linux64.tar.bz2
sudo mv arduino-cli-0.3.7-alpha.preview-linux64 /usr/local/bin/
sudo chown root:root /usr/local/bin/arduino-cli-0.3.7-alpha.preview-linux64
sudo ln -srfv /usr/local/bin/arduino-cli-0.3.7-alpha.preview-linux64 /usr/local/bin/arduino-cli

## Clone arduino-qt files

git clone https://bitbucket.org/Verlusti/arduino-qt.git
sudo mv arduino-qt /opt/
sudo chown -R root:root /opt/arduino-qt
sudo chmod +x /opt/arduino-qt/bin/qmake
sudo chmod +x /opt/arduino-qt/bin/make

Never add arduino-qt/bin to path!

## Setup Qt version for arduino-qt

If you have a qmake in you path, you don't need to do anything. 
If not, create a symlink in /usr/local/bin or home directory.

sudo ln -svf /opt/Qt/5.12.4/gcc_64/bin/qmake /usr/local/bin/arduino-qt-qmake-original
 -or-
ln -svf /opt/Qt/5.12.4/gcc_64/bin/qmake ~/.arduino-qt-qmake-original

## Setup arduino-qt Project

See example project.
Configure a kit using /opt/arduino-qt/bin/qmake as qmake.
In run configuration for arduino app projects add deployment step make -j1 install.

You can now compile and run Qt and Arduino applications next to each other in
subdir projects. Arduino projects will be opened in serial monitor.
