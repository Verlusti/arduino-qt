#!/bin/bash

SCRIPT_NAME=$(basename "$0")
SCRIPT_PATH=$(dirname "$0")

MY_DIR=$(pwd)
MY_USERNAME=$(whoami)
MY_HOME=$(getent passwd "$MY_USERNAME" | cut -f6 -d:)

PROJECT=$(basename -a -s .pro "$PRO_FILE");
PROJECT_DIR=$(dirname "$PRO_FILE")
CONFIG="$MY_HOME/.arduino-qt.config"

ARDUINO_LIB_DIR="$MY_HOME/Arduino/libraries"
ARDUINO_CLI=$(which arduino-qt-arduino-cli)
if [ -f "$MY_HOME/.arduino-qt-arduino-cli" ]; then
	ARDUINO_CLI="$MY_HOME/.arduino-qt-arduino-cli"
fi
if [ -z "$ARDUINO_CLI" ]; then 
	ARDUINO_CLI=$(which arduino-cli)
fi
ARDUINO_LIBS=$(sed 's/[[:space:]]*#.*//;/^[[:space:]]*$/d' "$PRO_FILE" | grep "ARDUINO_LIBS" | cut -f2 -d= | awk '{$1=$1};1')
ARDUINO_YT_LIB=$(sed 's/[[:space:]]*#.*//;/^[[:space:]]*$/d' "$PRO_FILE" | grep "ARDUINO_YT_LIB" | cut -f2 -d= | awk '{$1=$1};1')

arduino_auto_port()
{
	for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev); do
	(	
		syspath="${sysdevpath%/dev}"
		devname="$(udevadm info -q name -p $syspath)"
		[[ "$devname" == "bus/"* ]] && continue
		eval "$(udevadm info -q property --export -p $syspath)"
		[[ -z "$ID_SERIAL" ]] && continue
		
		if [ $(echo "$ID_SERIAL" | grep "Arduino" | wc -l) -gt 0 ]; then
			echo "/dev/$devname"
			return
		fi
		if [ $(echo "$ID_SERIAL" | grep "Industruino" | wc -l) -gt 0 ]; then
			echo "/dev/$devname"
			return
		fi
		if [ $(echo "$ID_SERIAL" | grep "FT232R" | wc -l) -gt 0 ]; then
			echo "/dev/$devname"
			return
		fi
	)
	done	
}

arduino_build_sha1sum()
{
	if [ -f "$PROJECT.bin" ] && [ -f "$PROJECT.elf" ]; then
		SHA_TMP=$(sha1sum "$PROJECT.bin" | cut -f1 -d' ')
		SHA_TMP+=$(sha1sum "$PROJECT.elf" | cut -f1 -d' ')
		echo -n "$SHA_TMP"
	fi
}

arduino_make_all()
{
	echo "Building $PROJECT ..."	
	echo $($ARDUINO_CLI version)
	$ARDUINO_CLI compile --debug --fqbn "$ARDUINO_CORE:$ARDUINO_BOARD" -o "$PROJECT" "$PROJECT_DIR"
	
	EXE_STRING=$(cat <<EOF
#!/bin/bash

echo -n "Getting port... "

MY_PORT="ARDUINO_PORT"
if [ -z "\$MY_PORT" ] || [ "\$MY_PORT" = "auto" ]; then
	MY_PORT=\$(PRO_FILE=MY_PRO_FILE MY_MAKE --auto-port)
fi

echo "\$MY_PORT @ ARDUINO_BAUDRATE baud"

gtkterm --port \$MY_PORT --speed ARDUINO_BAUDRATE

EOF
)
	EXE_STRING="${EXE_STRING//MY_MAKE/$0}"
	EXE_STRING="${EXE_STRING//MY_PRO_FILE/$PRO_FILE}"
	EXE_STRING="${EXE_STRING//ARDUINO_PORT/$ARDUINO_PORT}"
	EXE_STRING="${EXE_STRING//ARDUINO_BAUDRATE/$ARDUINO_BAUDRATE}"
	echo "$EXE_STRING" > "$PROJECT"
	chmod +x "$PROJECT"	
}

arduino_make_install() 
{
	echo "Installing $PROJECT ..."
	echo $($ARDUINO_CLI version)
	
	MY_PORT="$ARDUINO_PORT"
	if [ -z "$MY_PORT" ] || [ "$MY_PORT" = "auto" ]; then
		MY_PORT=$("$0" --auto-port)
	fi
	
	SHA_FILE=".last_upload_sha1sum"
	SHA_LAST=""
	if [ -f "$SHA_FILE" ]; then
		SHA_LAST=$(cat "$SHA_FILE")
	fi
	
	SHA_NOW=$(arduino_build_sha1sum)
	
	if [ -z "$SHA_LAST" ] || [ "$SHA_LAST" != "$SHA_NOW" ]; then
		$ARDUINO_CLI upload --fqbn "$ARDUINO_CORE:$ARDUINO_BOARD" -i "$PROJECT.bin" -p "$MY_PORT" -t
	fi	
	
	echo -n "$SHA_NOW" > "$SHA_FILE"
}

arduino_make_depends()
{
	CORES_INSTALLED=$($ARDUINO_CLI core list)
	if [ $(echo "$CORES_INSTALLED" | grep "$ARDUINO_CORE" | wc -l) -gt 0 ]; then
		VER_STRING=$(echo "$CORES_INSTALLED" | grep "$ARDUINO_CORE" | head -n1 | cut -d " " -f1 | cut -d "@" -f2)
		echo "Arduino core $ARDUINO_CORE $VER_STRING already installed."
	else				
		$ARDUINO_CLI core install "$ARDUINO_CORE"
	fi	

	LIBS_INSTALLED=$($ARDUINO_CLI lib list)
	for i in $ARDUINO_LIBS; do
		if [ ! -z "$i" ]; then
			echo "Checking for lib: $i"
			if [ $(echo "$LIBS_INSTALLED" | grep "$i" | wc -l) -gt 0 ]; then
				echo "Arduino library $i already installed."
			else				
				$ARDUINO_CLI lib install "$i"
			fi
		fi
	done
	
	PRI_FILE="$PROJECT_DIR/arduino-qt.pri"

	ARDUINO_INCLUDES=$(PRO_FILE="$PRO_FILE" "$0" --includes)
	if [ "$?" != 0 ]; then
		return
	fi

	PRI_STRING=""
	for i in $ARDUINO_INCLUDES
	do
		if [ ! -z "$i" ]; then
			PRI_STRING+="INCLUDEPATH += $i\n"
		fi
	done

	if [ "$ARDUINO_YT_LIB" = "true" ]; then
		PRI_STRING+="\n"
		
		TMP_HEADERS=$(find "$PROJECT_DIR" -name *.h)
		for i in $TMP_HEADERS
		do
			if [ ! -z "$i" ]; then
				BASE_NAME_H=$(basename -a -s .h "$i");
				DIR_NAME_H=$(dirname "$i")
				#if [ ! -f "$DIR_NAME_H/$BASE_NAME_H" ]; then
					#echo "#include \"$BASE_NAME_H.h\"" > "$DIR_NAME_H/$BASE_NAME_H"				
				#fi
				#PRI_STRING+="HEADERS += $DIR_NAME_H/$BASE_NAME_H\n"
			fi
		done
	fi

	echo -e -n "$PRI_STRING" > "$PRI_FILE"
}

arduino_make_clean()
{ 
	echo "Cleaning $PROJECT ..."
	rm -v "$PROJECT" "$PROJECT.bin" "$PROJECT.elf"
}

arduino_make_includes()
{
	if [ ! -f "$ARDUINO_CLI" ] || [ ! -f "$CONFIG" ] || [ ! -d "$ARDUINO_LIB_DIR" ]; then
		exit 1
	fi
	
	source "$CONFIG"
	
	CORE_PLATFORM=$(echo "$ARDUINO_CORE" | cut -f1 -d:)
	CORE_TYPE=$(echo "$ARDUINO_CORE" | cut -f2 -d:)
	if [ -z "$CORE_PLATFORM" ] || [ -z "CORE_TYPE" ]; then
		exit 1
	fi	

	CORE_INCLUDE_PATH="$MY_HOME/.arduino15/packages/$CORE_PLATFORM/hardware/$CORE_TYPE"

	LS_RET=$(ls "$CORE_INCLUDE_PATH")	
	if [ "$?" != 0 ] || [ -z "$LS_RET" ]; then
		exit 1
	fi
	LS_FIRST=$(echo "$LS_RET" | head -n1)
	
	CORE_INCLUDE_PATH+="/$LS_FIRST/cores/$CORE_PLATFORM"
	CORE_ARDUINO_HEADER="$CORE_INCLUDE_PATH/Arduino.h"

	if [ ! -f "$CORE_ARDUINO_HEADER" ]; then
		exit 1
	fi

	echo "$CORE_INCLUDE_PATH"
	
	for i in $ARDUINO_LIBS
	do
		if [ ! -z "$i" ]; then
			LIB_INCLUDE_PATH="$ARDUINO_LIB_DIR/$i/src"
			echo "$LIB_INCLUDE_PATH"
		fi
	done
	
	exit 0
}

arduino_make_ports()
{
	echo "Available serial ports"
	for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev); do
	(
		syspath="${sysdevpath%/dev}"
		devname="$(udevadm info -q name -p $syspath)"
		[[ "$devname" == "bus/"* ]] && continue
		eval "$(udevadm info -q property --export -p $syspath)"
		[[ -z "$ID_SERIAL" ]] && continue
		echo "/dev/$devname - $ID_SERIAL"
	)
	done
}

arduino_make_auto_port()
{
	COUNTER=0
	while [ $COUNTER -lt 5 ]
	do
		AUTO_PORT=$(arduino_auto_port)
		if [ ! -z "$AUTO_PORT" ]; then
			echo -n "$AUTO_PORT"
			exit 0
		fi
		sleep 1
		((COUNTER++))
	done
	
	exit 1
}

arduino_make_upgrade()
{
	$ARDUINO_CLI core update-index
	$ARDUINO_CLI core upgrade	
	$ARDUINO_CLI lib update-index
	$ARDUINO_CLI lib upgrade
}

if [ "$1" = "--includes" ]; then
	arduino_make_includes	
elif [ "$1" = "--ports" ]; then
	arduino_make_ports	
elif [ "$1" = "--auto-port" ]; then
	arduino_make_auto_port
elif [ "$1" = "--upgrade" ]; then
	arduino_make_upgrade
else
	if test -f "$PRO_FILE" ; then
		echo "Using .pro file '$PRO_FILE'."
	else
		echo "Missing .pro file '$PRO_FILE'."
		exit 1
	fi
fi

PROJECT_DATA=$(sed 's/[[:space:]]*#.*//;/^[[:space:]]*$/d' "$PRO_FILE")
ARDUINO_PROJECT_LIB=$(echo "$PROJECT_DATA" | grep "ARDUINO_PROJECT" | grep "library")
ARDUINO_PROJECT_APP=$(echo "$PROJECT_DATA" | grep "ARDUINO_PROJECT" | grep "app")

if [ ! -z "$ARDUINO_PROJECT_APP" ]; then 
	echo "Project is arduino app project."
elif [ ! -z "$ARDUINO_PROJECT_LIB" ]; then 
	echo "Project is arduino library project."
	if [ ! -d "$ARDUINO_LIB_DIR" ]; then
		mkdir -pv "$ARDUINO_LIB_DIR"
	fi
	ln -sfv "$PROJECT_DIR" "$ARDUINO_LIB_DIR"
	if [ "$1" != "depends" ]; then
		echo "Nothing to build for library. Quit."
		exit 0
	fi
else
	echo "Error: The project is not an arduino project."
	exit 1
fi

if [ ! -f "$CONFIG" ]; then
	CFG_STRING=$(cat <<EOF
ARDUINO_CORE=industruino:samd
ARDUINO_BOARD=industruino_d21g
ARDUINO_PORT=auto
ARDUINO_BAUDRATE=9600
EOF
)
	echo "$CFG_STRING" > "$CONFIG"
fi
source "$CONFIG" 

if [ ! -f "$ARDUINO_CLI" ] & [ ! -x "$(command -v "$ARDUINO_CLI")" ]; then
    echo "Arduino CLI '$ARDUINO_CLI' does not exist."
    exit 1;
fi

if [ -z "$ARDUINO_CORE" ]; then
	echo "Missing core parameter in config file."
	exit 1;
fi

if [ -z "$ARDUINO_BOARD" ]; then
	echo "Missing board parameter in config file."	
	exit 1;
fi

if [ -z "$ARDUINO_PORT" ]; then
	echo "Missing port parameter in config file."
	exit 1;
fi

if [ -z "$ARDUINO_BAUDRATE" ]; then
	echo "Missing baudrate parameter in config file or auto-detection failed."
	exit 1;
fi

case "$1" in
  all)
	arduino_make_all
    ;;
  install)
	arduino_make_install
    ;;
  depends)
	arduino_make_depends
	;;
  clean)
	arduino_make_clean	
    ;;
  *)
    echo "Usage: $SCRIPT_NAME {all|install|depends|clean|--includes|--ports|--auto-port|--upgrade}"
    exit 1
    ;;
esac

exit 0
